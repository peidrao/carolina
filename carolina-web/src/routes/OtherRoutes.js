import { BrowserRouter, Route, Switch } from "react-router-dom";

import Home from "../pages/Home";
import ListUsers from "../pages/users/ListUsers";
import SignRoutes from "./SignRoutes";
import RegisterUser from "../pages/RegisterUser";

const OtherRoutes = () => {
  return (
    <BrowserRouter>
      <Route path="/" component={Home} />
      <Route path="/users" component={ListUsers} />
      <Route path="/signup" component={RegisterUser} />
    </BrowserRouter>
  );
};

export default OtherRoutes;

import Authentication from "../pages/Login";
import RegisterUser from "../pages/RegisterUser";
import Home from "../pages/Home";

import { useAuth } from "../contexts/auth";
import ListUsers from "../pages/users/ListUsers";
import { Route, BrowserRouter } from "react-router-dom";
import DashBoard from "../pages/DashBoard";

const Routes = () => {
  const { signed } = useAuth();

  let routes;
  console.log(signed);

  routes = (
    <BrowserRouter>
      <Route exact path="/">
        <Home />
      </Route>
      <Route path="/users" component={ListUsers}>
        <ListUsers />
      </Route>
      <Route path="/dashboard" component={DashBoard}/>
      <Route path="/login" component={Authentication}  />
      <Route path="/signup" component={RegisterUser} />
    </BrowserRouter>
  );

  return routes;
};

export default Routes;

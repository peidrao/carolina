import {BrowserRouter, Route} from 'react-router-dom'

import Login from '../pages/Login'

const SignRoutes = () => {
    return (
        <BrowserRouter>
        <Route path="/login" component={Login} />
        </BrowserRouter>
    )
}


export default SignRoutes;
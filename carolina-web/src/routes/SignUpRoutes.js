import {BrowserRouter, Route} from 'react-router-dom'

import RegisterUser from '../pages/RegisterUser'

const SignUpRoutes = () => {
    return (
        <BrowserRouter>
        <Route path="/signup" component={RegisterUser} />
        </BrowserRouter>
    )
}


export default SignUpRoutes;
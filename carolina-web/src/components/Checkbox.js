const Checkbox = ({ value, label, name, onChange }) => {
  return (
    <div className="flex items-center">
    <input
      value={value}
      name={name}
      onChange={onChange}
      type="checkbox"
      className="h-4 w-4 text-indigo-600 focus:ring-indigo-500 border-gray-300 rounded"
    />
    <label
      className="ml-2 block text-sm text-gray-900"
    >
      {label}
    </label>
  </div>
  );
};

export default Checkbox;

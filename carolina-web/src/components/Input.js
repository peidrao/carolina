const Input = ({ value, label, name, placeholder, type, onChange }) => {
  return (
    <div>
       <label className="text-left">{label
       }</label>
        <input
          name={name}
          type={type}
          value={value}
          onChange={onChange}
          placeholder={onChange}
          className={
            "w-full p-2 text-primary border rounded-md outline-none text-sm transition duration-150 ease-in-out mb-4"
          }
        />
    </div>
  );
};

export default Input;

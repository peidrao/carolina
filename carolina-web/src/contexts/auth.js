import { createContext, useState, useContext, useEffect } from "react";
import api from "../services/api";

const AuthContext = createContext({});

export const AuthProvider = ({ children }) => {
  const [user, setUser] = useState();


  useEffect(() => {
    const storagedUser = localStorage.getItem("@App:user");
    const storagedToken = localStorage.getItem("@App:token");

    if (storagedToken && storagedToken) {
      // setUser(JSON.parse(storagedUser));
      setUser(storagedUser);
      api.defaults.headers.Authorization = `Bearer ${storagedToken}`;
    }
  }, []);

  async function Login(username, password) {
    const response = await api.post("/api/token/", { username, password });
    setUser(response.data.access);
    api.defaults.headers.Authorization = `Bearer ${response.data.access}`;

    //  localStorage.setItem('@App:user', JSON.stringify(response.data.user));
    localStorage.setItem('@App:user', response.data.access);
    localStorage.setItem('@App:token', response.data.access);
  }

  function Logout() {
    setUser(null);

    localStorage.removeItem("@App:user");
    localStorage.removeItem("App:token");
  }

  return (
    <AuthContext.Provider value={{ signed: Boolean(user), user, Login, Logout}}>
      {children}
    </AuthContext.Provider>
  );
};

export function useAuth() {
  const context = useContext(AuthContext);

  return context;
}

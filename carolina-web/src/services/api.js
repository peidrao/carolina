import axios from "axios";

const api = axios.create({ baseURL: "http://127.0.0.1:8000/" });

// api.interceptors.request.use(async (config) => {
//   const access = store.getState().auth.access;
//   if (access) {
//     config.headers.Authorization = `Bearer ${access}`;
//   }
//   return config;
// });

// api.interceptors.response.use(
//   (response) => {
//     statusFunction[response.status]();
//     return response;
//   },
//   (error) => {
//     statusFunction[error.response.status](error);
//     throw error;
//   }
// );

export default api;

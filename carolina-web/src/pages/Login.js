import { useState } from "react";
import {Link} from 'react-router-dom'
import Input from "../components/Input";
import imageLogin from "../asset/images/login_img.svg";
import {useAuth} from "../contexts/auth";

export default function Authentication() {
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();

  const context = useAuth()
  console.log(context)

  function handleSubmit(e) {
    e.preventDefault();
    context.Login(username, password)
  }

  return (
    <div className="w-full bg-red-800 h-screen grid grid-cols-2">
      <div className="bg-pink-900 flex items-center justify-center">
        <img src={imageLogin} alt="Imagem Login" className="h-80" />
      </div>

      <form
        className="bg-green-600 flex items-center justify-center flex-col"
        onSubmit={handleSubmit}
      >
        <h1 className="font-extrabold text-white text-4xl">Login</h1>
        <div className="w-1/2  flex flex-col gap-4">
          <Input
            type="text"
            label="Username"
            placeholder="Insert username"
            name="username"
            value={username}
            onChange={(event) => setUsername(event.target.value)}
          />
          <Input
            type="password"
            label="Password"
            placeholder="Insert password"
            name="username"
            value={password}
            onChange={(event) => setPassword(event.target.value)}
          />
          <button
            type="submit"
            className="p-3 text-white bg-black rounded w-full focus:outline-none"
          >
            Submit
          </button>
          <Link to="/signup">Register</Link>
        </div>
      </form>
    </div>
  );
}

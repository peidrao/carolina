import { useState, useEffect } from "react";
import api from "../../services/api";

export default function ListUsers() {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    api
      .get("/api/users/users/")
      .then((response) => setUsers(response.data))
      .catch((error) => console.log(error));
  }, []);

  return (
    <div className="w-full bg-blue-600 h-screen">
      <div className="bg-red-400 flex justify-center">
        <div >
          {users.map((user) => (
            <div key={user.id}>
              {user.name}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

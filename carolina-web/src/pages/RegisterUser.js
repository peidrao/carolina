import { useState } from "react";
import Input from "../components/Input";
import Swal from 'sweetalert2'
import Checkbox from "../components/Checkbox";
import api from "../services/api";

import { LockClosedIcon } from "@heroicons/react/solid";

export default function RegisterUser() {
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();
  const [name, setName] = useState();
  const [email, setEmail] = useState();

  const [is_staff, setIsStaff] = useState();
  const [is_superuser, setIssSuperUser] = useState();
  const [is_active, setIsActive] = useState();

  function handleSubmit(e) {
    e.preventDefault();
    console.log(e)

    api
      .post("/api/users/register/", {
        username,
        password,
        name,
        email,
        is_active,
        is_staff,
        is_superuser,
      })
      .then((response) => {
        setUsername("");
        setPassword("");
        setName("");
        setEmail("");
        setIsActive(false);
        setIsStaff(false);
        setIssSuperUser(false);

        Swal.fire({
          title: 'Sucesso',
          text: 'Usuário criado com sucesso',
          icon: 'success',
        })

        // history.push("/login");
      })
      .catch((error) => {
        Swal.fire({
          title: 'Erro!',
          text: error.response.data['text'],
          icon: 'error',
        })
      }
      );
  }

  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8">
      <div className="max-w-md w-full space-y-8">
        <div>
          {/* <img
            className="mx-auto h-12 w-auto"
            src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg"
            alt="Workflow"
          /> */}
          <h1 className="h-12 w-auto text-center text-6xl font-extrabold text-indigo-600">M</h1>
          <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
            Crie sua conta
          </h2>
        </div>
        <form className="mt-8 space-y-6" onSubmit={handleSubmit}>
          <div className="rounded-md shadow-sm -space-y-px">
            <Input
              type="text"
              label="Name"
              placeholder="Insert name"
              name="name"
              value={name}
              onChange={(event) => setName(event.target.value)}
            />
            <Input
              type="text"
              label="Username"
              placeholder="Insert username"
              name="username"
              value={username}
              onChange={(event) => setUsername(event.target.value)}
            />
            <Input
              type="email"
              label="E-mail"
              placeholder="Insert E-mail"
              name="email"
              value={email}
              onChange={(event) => setEmail(event.target.value)}
            />
            <Input
              type="password"
              label="Password"
              placeholder="Insert password"
              name="password"
              value={password}
              onChange={(event) => setPassword(event.target.value)}
            />
          </div>

          <div className="flex items-center justify-between">
            <Checkbox
              label="Super User"
              name="is_superuser"
              value={is_superuser}
              onChange={(event) => setIssSuperUser(event.target.checked)}
            />
            <Checkbox
              label="Active"
              name="is_active"
              value={is_active}
              onChange={(event) => setIsActive(event.target.checked)}
            />
            <Checkbox
              label="Staff"
              name="is_staff"
              value={is_staff}
              onChange={(event) => setIsStaff(event.target.checked)}
            />
          </div>

          <div className="text-sm flex items-end justify-end">
            <a
              href="#"
              className="font-medium text-indigo-600 hover:text-indigo-500"
            >
              Esqueceu sua senha?
            </a>
          </div>

          <div>
            <button
              type="submit"
              className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              <span className="absolute left-0 inset-y-0 flex items-center pl-3">
                <LockClosedIcon
                  className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400"
                  aria-hidden="true"
                />
              </span>
              Criar conta
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

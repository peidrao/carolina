import Routes from "./routes/index";
import {AuthProvider} from "./contexts/auth";

export default function App() {
  return (
    <AuthProvider>
      <Routes />
    </AuthProvider>
  );
}

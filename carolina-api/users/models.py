from django.db import models
from django.contrib.auth.models import AbstractUser


class BaseUser(AbstractUser):
    created_at = models.DateTimeField(auto_now_add=True)

class User(BaseUser):
    name = models.CharField(max_length=100, verbose_name='Nome', null=True)
    # cpf = models.CharField(max_length=11, unique=True, verbose_name='CPF', null=True)
    # rg = models.CharField(max_length=7, unique=True, verbose_name='RG', null=True)
    # profile_picture = models.ImageField(upload_to='images/profile_picture')
    # payment_method = models.ForeignKey(PaymentMethod, on_delete=models.CASCADE)
    # status = models.BooleanField(default=False)
    

    class Meta:
        ordering = ["-id"]


    def __str__(self):
        return self.name
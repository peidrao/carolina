import jwt

from django.conf import settings
from rest_framework import views, viewsets, generics, status, exceptions
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.hashers import make_password

from users.models import User
from users.serializers import (
    UserSerializer, UserUpdateSerializer, TokenUserSerializer, ChangePasswordSerializer)

from rest_framework.decorators import api_view, permission_classes


@api_view(['POST'])
def create_user(request):
    if request.data['password'] == '':
        message = {'text': 'Você precisa de uma senha!'}
        return Response(message, status=status.HTTP_400_BAD_REQUEST)
    try:
        user = User.objects.create(
            username=request.data['username'],
            name=request.data['name'],
            email=request.data['email'],
            password=make_password(request.data['password']),
            is_staff=request.data['is_staff'],
            is_active=request.data['is_active'],
            is_superuser=request.data['is_superuser']
        )
        serializer = UserSerializer(user, many=False)
        return Response(serializer.data)
    except:
            message = {'text': 'Email já está cadastrado'}
            return Response(message, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_users(request):
    users = User.objects.filter(is_active=True)
    serializer = UserSerializer(users, many=True)
    return Response(serializer.data)


@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def update_user(request):
    user = request.user
    serializer = UserSerializer(user, many=False)

    user.name = request.data['name']
    user.username = request.data['username']
    user.email = request.data['email']

    if request.data['password'] != '':
        user.password = make_password(request.data['password'])

    user.save()

    return Response(serializer.data)




class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_serializer_class(self):
        classes = {
            'GET': UserUpdateSerializer,
            'POST': UserSerializer,
            'PATCH': UserUpdateSerializer,
            'PUT': UserUpdateSerializer,
        }

        if self.request.method == 'POST':
            return classes[self.request.method]
        else:
            return classes[self.request.method]


class ChangePasswordViewSet(generics.UpdateAPIView):
    queryset = User.objects.all()
    serializer_class = ChangePasswordSerializer
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return User.objects.get(id=self.request.user.id)

    def update(self, request, *args, **kwargs):
        user = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            check = user.check_password(
                serializer.data.get("old_password")
            )

            if not check:
                return Response(
                    {"old_password": "Senha errada."},
                    status=status.HTTP_400_BAD_REQUEST,
                )

            user.set_password(serializer.data.get("new_password"))
            user.save()

            return Response(None, status=status.HTTP_204_NO_CONTENT)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TokenIsValidViewSet(views.APIView):
    def get(self, request):
        return Response(request.data)

    def post(self, request, *args, **kwargs):
        try:
            token = request.data["access"]
            decode = jwt.decode(token, settings.SECRET_KEY,
                                algorithms=["HS256"])
        except jwt.ExpiredSignatureError:
            raise exceptions.AuthenticationFailed('access_token expired')
        except jwt.InvalidSignatureError:
            raise exceptions.AuthenticationFailed('access_token invalid token')
        except KeyError:
            raise exceptions.AuthenticationFailed('access_token invalid key')

        user = TokenUserSerializer(User.objects.get(
            id=decode["user_id"]).values()[0])
        decode.update(user.data)
        return Response(decode)
